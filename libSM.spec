Name:           libSM
Version:        1.2.5
Release:        1
Summary:        Runtime library for X11 SM
License:        MIT
URL:            https://www.x.org
Source0:        https://www.x.org/releases/individual/lib/%{name}-%{version}.tar.xz
BuildRequires: make gcc
BuildRequires: pkgconfig(ice) >= 1.1.0
BuildRequires: pkgconfig(uuid)
BuildRequires: pkgconfig(xproto)
BuildRequires: pkgconfig(xtrans)

%description
The %{name} package contains runtime libraries for the X.Org X11 SM.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains development files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-libuuid
%make_build

%check
%make_build check

%install
%make_install
%delete_la

rm -rf %{buildroot}%{_datadir}/doc/%{name}

%files
%doc AUTHORS 
%license COPYING
%{_libdir}/*.so.*

%files          devel
%{_libdir}/*.so
%{_includedir}/X11/SM/SM*.h
%{_libdir}/*.a
%{_libdir}/pkgconfig/sm.pc

%files          help
%doc ChangeLog

%changelog
* Mon Dec 16 2024 Funda Wang <fundawang@yeah.net> - 1.2.5-1
- update to 1.2.5

* Wed Feb 08 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.2.4-1
- update to 1.2.4

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.2.3-5
- Rebuild for next release

* Tue Feb 9 2021 jinzhimin <jinzhimin2@huawei.com> - 1.2.3-4
- rebuild libSM

* Tue Feb 9 2021 jinzhimin <jinzhimin2@huawei.com> - 1.2.3-3
- add check in spec

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.3-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.3-1
- Package init
